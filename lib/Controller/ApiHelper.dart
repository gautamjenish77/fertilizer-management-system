import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class DatabaseHelper {
  String serverUrl = "http://ims.rarait.com/api";

  var status;

  var token;

  void addData(
    //String bag,
    //String quantity,
    String bank_id,
    String purpose,
    String notes,
    String action_date,
    //String user_id,
    String item,
    String consumer_name,
    String consumer_phone,
    String consumer_address_id,
    String consumer_citizen_id,
    String consumer_area,
    String rate,
    String amount,
    String kg,

  ) async {
    final prefs = await SharedPreferences.getInstance();
    final key = 'token';
    final value = prefs.get(key) ?? 0;

    String myUrl = "http://ims.rarait.com/api/sales";
    http.post(myUrl, headers: {
      'Accept': 'application/json',
      'Authorization':
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiNTFkODQwZDhhYTNmZTUxYzhlOWE4M2E0OWYzODMwMTkzNzgxNGQ5ZjZlYmM1MmE4M2UyMzNkZjFjZTZlNGU0ZGQ0ZmM5Yjg1MzBhMjJiYTMiLCJpYXQiOjE2MTE0ODYyMTQsIm5iZiI6MTYxMTQ4NjIxNCwiZXhwIjoxNjQzMDIyMjE0LCJzdWIiOiI2NjAiLCJzY29wZXMiOltdfQ.Av7RpBzcOdy6qsCoXr9asDM43uKMTh4axbG7ATynodQfg373ZFsUa8LM_mm5TIZn6RIWOV8AW4wtFVgZqsvTZFFStA0hDwgP9Cfd6nbXJ0BxCvJGTbv_jEGO0HuQHGsOT4GDKPkI0Bt6OHStefYj6MR6WU90F-AVcu5FQ1Fr6TOqoeBulUiYSV8X-OshLSRVycx5mHqq3C6Hquz3yJuLVHzD4zmFQCNp-gUYcKylS1_wI0ch1xWmuR_jOarKe-BB2YQOq3d25PfHxG5F0OeDbLhem9xVzxXS1KitFl3pZlMSl1b0e1XFxHKbqS2Ikj_5EteCFbVZqZGza0xBIxfQcmG2a_dtHUDjKSDk8_wZgh5GyOy1Y00ZHSn33Z5r4QwZPkK-RLHEtS69rpbxxf2uiTEaPRjyZg2aZOCVRkUXCAZfuGSl5JfuwYuq_SjPK6MH04_l49q5Grsa-CiJu2y288U8HGXUvt5vjbJAXJC06b04bcjxBDKhuFIEH4Y2rjQi6ej_LcJ5RQ__S-B3_y7ucSykVBU6bPDr_40bbwlTiuxvG5YXLZDs4qYjPJG4qjdtN2rhsqddwf3aqeQ9ACSEEOItKDiVvtw_1DHItvxCWvQujZ9FAituTPXNxO1vp7JiQ25glc2tmP1DaaPIcwK5xIJvjZ-ZfGooVe4yXsXV5Ew',}, body: {
      //"bag": "$bag",
      //"quantity": "$quantity",
      "bank_id": "$bank_id",
      "purpose": "$purpose",
      "notes": "$notes",
      "action_date": "$action_date",
      //"user_id": "$user_id",
      "item": "$item",
      "consumer_name": "$consumer_name",
      "consumer_phone": "$consumer_phone",
      "consumer_address_id": "$consumer_address_id",
      "consumer_citizen_id": "$consumer_citizen_id",
      "consumer_area": "$consumer_area",
      "rate": "$rate",
      "amount": "$amount",
      "kg": "$kg",
    }).then((response) {
      print('Response status : ${response.statusCode}');
      print('Response body : ${response.body}');
    });
  }

  Future<Map<String, dynamic>> getData() async {
    final prefs = await SharedPreferences.getInstance();
    final key = 'token';
    final value = prefs.get(key) ?? 0;

    String myUrl = "$serverUrl/sales";
    http.Response response = await http.get(myUrl, headers: {
      'Accept': 'application/json',
      'Authorization':
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiNTFkODQwZDhhYTNmZTUxYzhlOWE4M2E0OWYzODMwMTkzNzgxNGQ5ZjZlYmM1MmE4M2UyMzNkZjFjZTZlNGU0ZGQ0ZmM5Yjg1MzBhMjJiYTMiLCJpYXQiOjE2MTE0ODYyMTQsIm5iZiI6MTYxMTQ4NjIxNCwiZXhwIjoxNjQzMDIyMjE0LCJzdWIiOiI2NjAiLCJzY29wZXMiOltdfQ.Av7RpBzcOdy6qsCoXr9asDM43uKMTh4axbG7ATynodQfg373ZFsUa8LM_mm5TIZn6RIWOV8AW4wtFVgZqsvTZFFStA0hDwgP9Cfd6nbXJ0BxCvJGTbv_jEGO0HuQHGsOT4GDKPkI0Bt6OHStefYj6MR6WU90F-AVcu5FQ1Fr6TOqoeBulUiYSV8X-OshLSRVycx5mHqq3C6Hquz3yJuLVHzD4zmFQCNp-gUYcKylS1_wI0ch1xWmuR_jOarKe-BB2YQOq3d25PfHxG5F0OeDbLhem9xVzxXS1KitFl3pZlMSl1b0e1XFxHKbqS2Ikj_5EteCFbVZqZGza0xBIxfQcmG2a_dtHUDjKSDk8_wZgh5GyOy1Y00ZHSn33Z5r4QwZPkK-RLHEtS69rpbxxf2uiTEaPRjyZg2aZOCVRkUXCAZfuGSl5JfuwYuq_SjPK6MH04_l49q5Grsa-CiJu2y288U8HGXUvt5vjbJAXJC06b04bcjxBDKhuFIEH4Y2rjQi6ej_LcJ5RQ__S-B3_y7ucSykVBU6bPDr_40bbwlTiuxvG5YXLZDs4qYjPJG4qjdtN2rhsqddwf3aqeQ9ACSEEOItKDiVvtw_1DHItvxCWvQujZ9FAituTPXNxO1vp7JiQ25glc2tmP1DaaPIcwK5xIJvjZ-ZfGooVe4yXsXV5Ew',});
    return json.decode(response.body);
  }

  _save(String token) async {
    final prefs = await SharedPreferences.getInstance();
    final key = 'token';
    final value = token;
    prefs.setString(key, value);
  }

  read() async {
    final prefs = await SharedPreferences.getInstance();
    final key = 'token';
    final value = prefs.get(key) ?? 0;
    print('read : $value');
  }
}
