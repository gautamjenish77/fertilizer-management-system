class User {
  int id;
  String name;
  String phone;

  User({
    this.id,
    this.name,
    this.phone,
});

  factory User.fromJson(Map<String, dynamic> parsedJson) {
    return User(
      id: parsedJson["id"],
      name: parsedJson["name"] as String,
      phone: parsedJson["phone"] as String,
    );
  }
}