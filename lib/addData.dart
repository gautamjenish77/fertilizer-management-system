/*import 'package:dropdownfield/dropdownfield.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:imss/Controller/ApiHelper.dart';
import 'package:flutter/services.dart';
import 'package:imss/pages/app_main.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:imss/FarmerAutoComplete/user.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class AddData extends StatefulWidget {
  AddData({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _AddDataState createState() => _AddDataState();
}

class _AddDataState extends State<AddData> {
//Multiple Checkbox here
  String value = "";

  AutoCompleteTextField searchTextField;
  GlobalKey<AutoCompleteTextFieldState<User>> key = new GlobalKey();
  static List<User> users = new List<User>();
  bool loading = true;

  void getUsers() async {
    try {
      String myUrl = "http://ims.rarait.com/api/users";
      final response = await http.get(myUrl, headers: {
        "Accept": "application/json",
        "Authorization":
            "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiMTFmYjI0NWZmYmUwMDJkZTY0YzI4ZmQ1ZWRlMzg1NzViNDZjNTVkNTQ5YTRjNWQwZGEyZTFkZmRiYWY3NDM1OGQzYmY1YzlkNThlOGY1NzYiLCJpYXQiOjE2MTEyMjE1NjQsIm5iZiI6MTYxMTIyMTU2NCwiZXhwIjoxNjQyNzU3NTY0LCJzdWIiOiI2NjAiLCJzY29wZXMiOltdfQ.aL7gVfAX_pGxXBjS2ur0wIAG_gN_u9VTbO6CkI2HvFYXsStZifqh2oOtOF92ijfYpeIMJmA3NDvWpyqYCUHfxIQls2SdaTF16BWrbd-UCsxK4OWRTvUpb7tI6QaGRlC2Dg9SyMeUQDmxSS8_3hO3NE4O3XCF5Plgj4EtFPxZDjF-NgIO-QY1EilKREqIad84Kex5YuGz3Hivli41ujDlbcW6mbdnhyKUtrLzlbPREMqI391PJwdeTPrxJ9BKps1eMkhwUglhZzNIHdifk-mjw0dWfbVfFv4r8b3Kh0hDyvoxBMuyuVcY4c2JJzbWrAmvCLR9ETKYbi6RDGE7q6BojFOiMYLTV4w_QthcBb5fdoU7Kd0sIlC7VRwW1SP5nEzxGiXj-1k5LaVAx7xLgYm_sCrByUgTH8Kkkl4B-RBjOW5Vj_V0jhj0yRJV72iCSUZZ79JnHkLhzCn3BLBy8ipjdkfcG-Ly5J9_Opxdmv7ytJ6CzO4ai-Pg59O6h009LWz3cGDU4Jx4Af0SDMeznoAXqWaLYy0exNtbkrUJATIMpRI-F_vAbIykRHJNPHhHATlYg5pVwkNFCOpI0C_fdqJAfqa0IhVYj3C2CvBxAUVY8ZC1z19ZXY0fo84AERTgAjiXoRQp2fPSDG7Ts7WhtoU08YhZBkMXgPcNp471wuuP3lw"
      });
      if (response.statusCode == 200) {
        users = loadUsers(response.body);
        print('Users: ${users.length}');
        setState(() {
          loading = false;
        });
      } else {
        print("Error getting users");
      }
    } catch (e) {
      print("Error getting users");
    }
  }

  static List<User> loadUsers(String jsonString) {
    final parsed = json.decode(jsonString).cast<Map<String, dynamic>>();
    return parsed.map<User>((json) => User.fromJson(json)).toList();
  }

  @override
  void initState() {
    getUsers();
    super.initState();
  }

  Widget row(User user) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          user.name,
          style: TextStyle(fontSize: 16.0),
        ),
        SizedBox(
          width: 10.0,
        ),
        Text(
          user.phone,
        ),
      ],
    );
  }

  bool rice = false;
  bool wheat = false;
  bool maize = false;
  bool mustard = false;
  bool others = false;

  /*void onChangedRice(bool value) {
    setState(() {
      rice = value;
    });
  }

  void onChangedWheat(bool value) {
    setState(() {
      wheat = value;
    });
  }

  void onChangedMaize(bool value) {
    setState(() {
      maize = value;
    });
  }

  void onChangedMustard(bool value) {
    setState(() {
      mustard = value;
    });
  }

  void onChangedOthers(bool value) {
    setState(() {
      others = value;
    });
  }*/

  DatabaseHelper databaseHelper = new DatabaseHelper();

  final TextEditingController _bagController = new TextEditingController();

  final TextEditingController _quantityController = new TextEditingController();
  final TextEditingController _bank_idController = new TextEditingController();
  final TextEditingController _purposeController = new TextEditingController();
  final TextEditingController _notesController = new TextEditingController();
  final TextEditingController _action_dateController =
      new TextEditingController();
  final TextEditingController _user_idController = new TextEditingController();
  final TextEditingController _itemController = new TextEditingController();
  final TextEditingController _consumer_nameController =
      new TextEditingController();
  final TextEditingController _consumer_phoneController =
      new TextEditingController();
  final TextEditingController _consumer_address_idController =
      new TextEditingController();
  final TextEditingController _consumer_citizen_idController =
      new TextEditingController();

  // ignore: non_constant_identifier_names
  final TextEditingController _consumer_areaController =
      new TextEditingController();
  final TextEditingController _rateController = new TextEditingController();
  final TextEditingController _amountController = new TextEditingController();
  final TextEditingController _kgController = new TextEditingController();

  GlobalKey<FormState> salesformkey = GlobalKey<FormState>();

  List numberList = [
    "9842049245",
    "9842365122",
    "9842181991",
    "9842198093",
    "9810310253",
    "9819068807",
    "9842176429",
    "9842585129",
    "9842091919",
    "9805384209",
    "9811039977",
    "9808076397",
    "9812396164",
    "9827023481",
    "9882222959",
    "9825357975",
    "9813033463",
    "9813072296",
    "9819086452",
    "9827357827",
    "9817318116",
    "9805327043",
    "9814321932",
    "9811058063",
    "9842136116",
    "9852080873",
    "9817238355",
    "9862143854",
    "9817375924",
    "9818315781",
    "9842086406",
    "9842113103",
    "9842071906",
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Add Sales",
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.home,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => new FirstPage()));
            },
          ),
          backgroundColor: Color(0xFF1B5E20),
          title: Text('Add Sales'),
        ),
        body: Container(
          padding: const EdgeInsets.symmetric(vertical: 25.0, horizontal: 27.0),
          child: Form(
            key: salesformkey,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  TextFormField(
                    controller: _bagController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'No. of Bags',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    controller: _quantityController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Quantity in Metric Ton (M.T)',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    controller: _rateController,
                    cursorColor: Colors.green,
                    decoration: InputDecoration(
                      labelText: 'Rate',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    controller: _amountController,
                    cursorColor: Colors.green,
                    decoration: InputDecoration(
                      labelText: 'Amount',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _bank_idController,
                    cursorColor: Colors.indigo,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Bank ID / Managable ID',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    child: Text(
                      'Purpose',
                      style: TextStyle(
                        color: Colors.green[900],
                        fontSize: 16.0,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                 Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      checkbox(
                          title: "rice",
                          initValue: rice,
                          onChanged: (sts) => setState(() => rice = sts)),
                      SizedBox(
                        width: 15.0,
                      ),
                      checkbox(
                          title: "wheat",
                          initValue: wheat,
                          onChanged: (sts) => setState(() => wheat = sts)),
                      SizedBox(
                        width: 15.0,
                      ),
                      checkbox(
                          title: "maize",
                          initValue: maize,
                          onChanged: (sts) => setState(() => maize = sts)),
                      SizedBox(
                        width: 15.0,
                      ),
                      checkbox(
                          title: "mustard",
                          initValue: mustard,
                          onChanged: (sts) => setState(() => mustard = sts)),
                      SizedBox(
                        width: 15.0,
                      ),
                      checkbox(
                          title: "others",
                          initValue: others,
                          onChanged: (sts) => setState(() => others = sts)),
                    ],
                  ),
                  /*Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: rice,
                      onChanged: onChangedRice,
                      activeColor: Colors.green[900],
                      title: const Text("Rice"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: wheat,
                      onChanged: onChangedWheat,
                      activeColor: Colors.green[900],
                      title: const Text("Wheat"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: maize,
                      onChanged: onChangedMaize,
                      activeColor: Colors.green[900],
                      title: const Text("Maize"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: mustard,
                      onChanged: onChangedMustard,
                      activeColor: Colors.green[900],
                      title: const Text("Mustard"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: others,
                      onChanged: onChangedOthers,
                      activeColor: Colors.green[900],
                      title: const Text("Others"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),*/
                 /* TextFormField(
                    controller: _purposeController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Purpose',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),*/
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    controller: _notesController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Notes',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _action_dateController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Date',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _user_idController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: "User's ID",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),

                  /*Container(
                    child: DropdownButton<String>(
                      items: [
                        DropdownMenuItem<String>(
                          value: "1",
                          child: Center(
                            child: Text("Urea"),
                          ),
                        ),
                        DropdownMenuItem<String>(
                          value: "2",
                          child: Center(
                            child: Text("DAP"),
                          ),
                        ),
                        DropdownMenuItem<String>(
                          value: "3",
                          child: Center(
                            child: Text("Potash"),
                          ),
                        ),
                        DropdownMenuItem<String>(
                          value: "4",
                          child: Center(
                            child: Text("Chun"),
                          ),
                        )
                      ],
                      onChanged: (_value) => {
                        print(_value.toString()),
                        setState(() {
                          value = _value;
                        })
                      },
                      hint: Text("Select Commodity"),
                    ),
                  ),

                  Text(
                    "$value",
                  ),*/

                  TextFormField(
                    controller: _itemController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Items ID (1/2/3/4)',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  TextFormField(
                    controller: _kgController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Enter KG',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  /*Container(
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Color.fromRGBO(97, 112, 228, 1),
                        ),
                        borderRadius: BorderRadius.circular(10.0)),
                    child: DropDownField(
                      labelStyle: const TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w400,
                      ),
                      labelText: 'Commodity',
                      controller: comodityselected,
                      hintText: "Choose one",
                      enabled: true,
                      itemsVisibleInDropdown: 4,
                      items: Commodity,
                      onValueChanged: (value) {
                        setState(() {
                          choose = value;
                        });
                      },
                    ),
                  ),*/
                  /* AutoCompleteTextField(
                    controller: _itemController,
                    itemSubmitted: (item) {
                      _itemController.text = item;
                    },
                    clearOnSubmit: false,
                    suggestions: [
                      "1",
                      "2",
                      "3",
                      "4",
                    ],
                    style: TextStyle(color: Colors.black, fontSize: 16.0),
                    itemBuilder: (context, item) {
                      return Container(
                        padding: EdgeInsets.all(20.0),
                        child: Row(
                          children: [
                            Text(
                              item,
                              style: TextStyle(color: Colors.black),
                            ),
                          ],
                        ),
                      );
                    },
                    itemSorter: (c, d) {
                      return c.compareTo(d);
                    },
                    itemFilter: (item, query) {
                      return item.toLowerCase().startsWith(query.toLowerCase());
                    },
                    decoration: InputDecoration(
                      labelText: "Commodity",
                      hintText: "'1.Dap','2.Urea','3.Potash','4.Chun'",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),

                  ),*/
                  SizedBox(
                    height: 10.0,
                  ),
                  // ignore: missing_required_param
                  AutoCompleteTextField(
                    controller: _consumer_phoneController,
                    itemSubmitted: (item) {
                      _consumer_phoneController.text = item;
                    },
                    clearOnSubmit: false,
                    suggestions: numberList,
                    style: TextStyle(color: Colors.black, fontSize: 16.0),
                    itemBuilder: (context, item) {
                      return Container(
                        padding: EdgeInsets.all(20.0),
                        child: Row(
                          children: [
                            Text(
                              item,
                              style: TextStyle(color: Colors.black),
                            ),
                          ],
                        ),
                      );
                    },
                    itemSorter: (c, d) {
                      return c.compareTo(d);
                    },
                    itemFilter: (item, query) {
                      return item.toLowerCase().startsWith(query.toLowerCase());
                    },
                    decoration: InputDecoration(
                      labelText: "Consumer's Phone",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ), //key: null,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      loading
                          ? CircularProgressIndicator()
                          : searchTextField = AutoCompleteTextField<User>(
                              key: key,
                              clearOnSubmit: false,
                              suggestions: users,
                              style: TextStyle(
                                  color: Colors.black, fontSize: 16.0),
                              decoration: InputDecoration(
                                labelText: "Consumer's Name",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                              ),
                              itemFilter: (item, query) {
                                return item.name
                                    .toLowerCase()
                                    .startsWith(query.toLowerCase());
                              },
                              itemSorter: (a, b) {
                                return a.name.compareTo(b.name);
                              },
                              itemSubmitted: (item) {
                                setState(() {
                                  searchTextField.textField.controller.text =
                                      item.name;
                                });
                              },
                              itemBuilder: (context, item) {
                                //ui for the autocomplete row
                                return row(item);
                              },
                            ),
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _consumer_address_idController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: "Consumer's Address ID",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _consumer_citizen_idController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: "Consumer's Citizen No.",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _consumer_areaController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: "Consumer's Area Code",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    height: 50,
                    child: new FlatButton(
                      onPressed: () {
                        databaseHelper.addData(
                            _bagController.text.trim(),
                            _quantityController.text.trim(),
                            _bank_idController.text.trim(),
                            _purposeController.text.trim(),
                            _notesController.text.trim(),
                            _action_dateController.text.trim(),
                            _user_idController.text.trim(),
                            _itemController.text.trim(),
                            _consumer_nameController.text.trim(),
                            _consumer_phoneController.text.trim(),
                            _consumer_address_idController.text.trim(),
                            _consumer_citizen_idController.text.trim(),
                            _consumer_areaController.text.trim(),
                            _rateController.text.trim(),
                            _amountController.text.trim(),
                            _kgController.text.trim());


                        print("Rice in ${rice ? 'checked' : 'unchecked'}");
                        print("Wheat in ${wheat ? 'checked' : 'unchecked'}");
                        print("Maize in ${maize ? 'checked' : 'unchecked'}");
                        print(
                            "Mustard in ${mustard ? 'checked' : 'unchecked'}");
                        print("Others in ${others ? 'checked' : 'unchecked'}");
                      },
                      color: Colors.green,
                      child: new Text(
                        "Add",
                        style: new TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget checkbox(
      {String title, bool initValue, Function(bool boolValue) onChanged}) {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Text(
        title,
        style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
      ),
      Checkbox(
        value: initValue,
        onChanged: (b) => onChanged(b),
        activeColor: Color(0xFF1B5E20),
      )
    ]);
  }


}

String select = "";*/

/*String choose = "";

final comodityselected = TextEditingController();
// creating a list of strings for commodities
List<String> Commodity = [
  "1",
  "2",
  "3",
  "4",
];*/
