/*import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/material.dart';
import 'package:imss/Controller/ApiHelper.dart';
import 'package:flutter/services.dart';
import 'package:imss/pages/app_main.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:imss/FarmerAutoComplete/user.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:imss/models/purpose.dart';

class AddData extends StatefulWidget {
  AddData({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _AddDataState createState() => _AddDataState();
}

class _AddDataState extends State<AddData> {
//Multiple CheckBox here

  String value = "";

  /*Commodity selectedCommodity;
  List<Commodity> commodities = <Commodity>[
    const Commodity(1, 'Urea'),
    const Commodity(2, 'DAP'),
    const Commodity(3, 'Potash'),
    const Commodity(4, 'Chun')
  ];*/

  // String valueChoose;
  // List listItem = ["Urea", "DAP", "Potash", "Chun"];

//Date here
  String title = 'Date Picker';
  DateTime _date = DateTime.now();

  Future<Null> _selectDate(BuildContext context) async {
    DateTime _datePicker = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: DateTime(2000),
        lastDate: DateTime(2030),
        textDirection: TextDirection.ltr,
        initialDatePickerMode: DatePickerMode.day,
        //selectableDayPredicate: (DateTime val) => val.weekday ==6 || val.weekday == 7 ? false : true,
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData(
              primarySwatch: Colors.green,
              primaryColor: Color.fromRGBO(97, 112, 228, 1),
              accentColor: Color.fromRGBO(97, 112, 228, 1),
            ),
            child: child,
          );
        });
    if (_datePicker != null && _datePicker != _date) {
      setState(() {
        _date = _datePicker;
        print(
          _date.toString(),
        );
      });
    }
  }

  AutoCompleteTextField searchTextField;
  GlobalKey<AutoCompleteTextFieldState<User>> key = new GlobalKey();
  static List<User> users = new List<User>();
  bool loading = true;

  void getUsers() async {
    try {
      String myUrl = "http://ims.rarait.com/api/users";
      final response = await http.get(myUrl, headers: {
        "Accept": "application/json",
        "Authorization":
            "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiOWQ5ZjViNDkxNzQ3YTI3YzU1MGVjMDFmMzdmZDFlMGI3NzI1YTQzZDQyZjY3ZmQ4ZTBhMzIzYzc3MDZjZDQ1NzhkMzNkOTUwYmMxZTFiY2IiLCJpYXQiOjE2MDk3NDU5MDQsIm5iZiI6MTYwOTc0NTkwNCwiZXhwIjoxNjQxMjgxOTA0LCJzdWIiOiI2NjAiLCJzY29wZXMiOltdfQ.ZmjoCT8jRnCYI8GKXpdurNtv1rKGHHe6P92oNhBL_c6BMXynedEjM-TJaJEQNwnRmsMx43Ql3Wck62Xpqix45afXZETwMfLgtuydUszM-F2HoBs1w_4AX-aYkk2Vix3sZMAQHoqC0E5tVBww-E_0ltsUMZHYPM44vEUqMWi38M4OjFnYof7eEEKS_aD7FMvoC2Ev0Yulki1UweGT75eZihrymmzMe2VGSbw1aZjKHpjb3lhiSwczVTxx5OCAe3m1tsRsLSCqv3mCVqi9KyFD8WQey98HxvbijwJnl1SUovOO0F3RDyj-xXmDh6nRY0FDFCfl697Ig-bYdN6t824oRXDz0gLdjoB-Oup-tAMKU1IXY4YHf2gdWdgWhAESbzSSms0rYpDjvJEYZMhjb_BhqQL3VC64hjZFh5VifZxxZH6tBiApxWP21n5W8EUbsKIIbcYaScm_nCCtXCi4M3pzvHr5llBkR3-cdWXFu5m-5_62te6CqtMcneZVU_ov3PBrkqNj0HjdzA9B28QCrIpUVXsn6WPpL9_nle5HP1F_vDu46aDGWpAPE5LrAiSZ2Euy-v7w0FXyh1W_CZ6Y1P8pC1ZbTpCdFDruA_StYwFqOyHWzsMzDj7W95DjtMjnewiMyXi3vv3ChjQBKTBL5Jx2QyR2L9ueQRKBqk1A3krl2LA"
      });
      if (response.statusCode == 200) {
        users = loadUsers(response.body);
        print('Users: ${users.length}');
        setState(() {
          loading = false;
        });
      } else {
        print("Error getting users");
      }
    } catch (e) {
      print("Error getting users");
    }
  }

  static List<User> loadUsers(String jsonString) {
    final parsed = json.decode(jsonString).cast<Map<String, dynamic>>();
    return parsed.map<User>((json) => User.fromJson(json)).toList();
  }

  @override
  void initState() {
    getUsers();
    super.initState();
    // selectedCommodity = commodities[0];
  }

  Widget row(User user) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          user.name,
          style: TextStyle(fontSize: 16.0),
        ),
        SizedBox(
          width: 10.0,
        ),
        Text(
          user.phone,
        ),
      ],
    );
  }

  bool rice = false;
  bool wheat = false;
  bool maize = false;
  bool mustard = false;
  bool others = false;

  /*void onChangedRice(bool value) {
    setState(() {
      rice = value;
    });
  }

  void onChangedWheat(bool value) {
    setState(() {
      wheat = value;
    });
  }

  void onChangedMaize(bool value) {
    setState(() {
      maize = value;
    });
  }

  void onChangedMustard(bool value) {
    setState(() {
      mustard = value;
    });
  }

  void onChangedOthers(bool value) {
    setState(() {
      others = value;
    });
  }*/

  DatabaseHelper databaseHelper = new DatabaseHelper();

  final TextEditingController _bagController = new TextEditingController();

  final TextEditingController _quantityController = new TextEditingController();
  final TextEditingController _bank_idController = new TextEditingController();
  final TextEditingController _purposeController = new TextEditingController();
  final TextEditingController _notesController = new TextEditingController();
  final TextEditingController _action_dateController =
      new TextEditingController();
  final TextEditingController _user_idController = new TextEditingController();
  final TextEditingController _itemController = new TextEditingController();
  final TextEditingController _consumer_nameController =
      new TextEditingController();
  final TextEditingController _consumer_phoneController =
      new TextEditingController();
  final TextEditingController _consumer_address_idController =
      new TextEditingController();
  final TextEditingController _consumer_citizen_idController =
      new TextEditingController();

  // ignore: non_constant_identifier_names
  final TextEditingController _consumer_areaController =
      new TextEditingController();
  final TextEditingController _rateController = new TextEditingController();
  final TextEditingController _amountController = new TextEditingController();
  final TextEditingController _kgController = new TextEditingController();

  GlobalKey<FormState> salesformkey = GlobalKey<FormState>();

  List numberList = [
    "9842049245",
    "9842365122",
    "9842181991",
    "9842198093",
    "9810310253",
    "9819068807",
    "9842176429",
    "9842585129",
    "9842091919",
    "9805384209",
    "9811039977",
    "9808076397",
    "9812396164",
    "9827023481",
    "9882222959",
    "9825357975",
    "9813033463",
    "9813072296",
    "9819086452",
    "9827357827",
    "9817318116",
    "9805327043",
    "9814321932",
    "9811058063",
    "9842136116",
    "9852080873",
    "9817238355",
    "9862143854",
    "9817375924",
    "9818315781",
    "9842086406",
    "9842113103",
    "9842071906",
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Add Sales",
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.home,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => new FirstPage()));
            },
          ),
          backgroundColor: Color(0xFF1B5E20),
          title: Text('Add Sales'),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.refresh,
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new AddData()));
              },
            ),
          ],
        ),
        body: Container(
          padding: const EdgeInsets.symmetric(vertical: 25.0, horizontal: 27.0),
          child: Form(
            key: salesformkey,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    child: Text(
                      'Purpose',
                      style: TextStyle(
                        color: Colors.green[900],
                        fontSize: 16.0,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _purposeController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Purpose',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  /*Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      checkbox(
                          title: "rice",
                          initValue: rice,
                          onChanged: (sts) => setState(() => rice = sts)),
                      SizedBox(
                        width: 15.0,
                      ),
                      checkbox(
                          title: "wheat",
                          initValue: wheat,
                          onChanged: (sts) => setState(() => wheat = sts)),
                      SizedBox(
                        width: 15.0,
                      ),
                      checkbox(
                          title: "maize",
                          initValue: maize,
                          onChanged: (sts) => setState(() => maize = sts)),
                      SizedBox(
                        width: 15.0,
                      ),
                      checkbox(
                          title: "mustard",
                          initValue: mustard,
                          onChanged: (sts) => setState(() => mustard = sts)),
                      SizedBox(
                        width: 15.0,
                      ),
                      checkbox(
                          title: "others",
                          initValue: others,
                          onChanged: (sts) => setState(() => others = sts)),
                    ],
                  ),*/
                  SizedBox(
                    height: 10.0,
                  ),
                  /*TextFormField(
                    controller: _itemController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText:
                            'Items ID (1 : Urea || 2 : DAP || 3 : Potash || 4 : Chun)',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),*/

                  /*Container(
                    padding: const EdgeInsets.symmetric(
                      vertical: 8.0,
                      horizontal: 20.0,
                    ),
                    decoration: BoxDecoration(
                      border: Border.all(
                        // color: Color.fromRGBO(97, 112, 228, 1),
                        color: Colors.blue,
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: DropdownButton(
                      hint: Text("Select Commodity"),
                      dropdownColor: Colors.white,
                      icon: Icon(Icons.arrow_drop_down),
                      iconSize: 36,
                      isExpanded: true,
                      style: TextStyle(color: Colors.black),
                      value: valueChoose,
                      onChanged: (newValue) {
                        setState(() {
                          valueChoose = newValue;
                        });
                      },
                      items: listItem.map((valueItem) {
                        return DropdownMenuItem(
                          value: valueItem,
                          child: Text(valueItem),
                        );
                      }).toList(),
                    ),
                  ),*/

                  /*Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20.0,
                    ),
                    decoration: BoxDecoration(
                      border: Border.all(
                          //color: Color.fromRGBO(97, 112, 228, 1),
                          color: Colors.green),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: DropdownButton<Commodity>(
                      //isExpanded: true,
                      value: selectedCommodity,
                      onChanged: (Commodity newValue) {
                        setState(() {
                          selectedCommodity = newValue;
                        });
                      },
                      items: commodities.map((Commodity user) {
                        return new DropdownMenuItem<Commodity>(
                          value: user,
                          child: new Text(
                            user.name,
                            style: new TextStyle(color: Colors.black),
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  new Text(
                      "Selected Commodity name is ${selectedCommodity.name} : and Id is : ${selectedCommodity.id}"),*/

                  DropdownButton<String>(
                    items: [
                      DropdownMenuItem<String>(
                        value: "1",
                        child: Center(
                          child: Text("Urea"),
                        ),
                      ),
                      DropdownMenuItem<String>(
                        value: "2",
                        child: Center(
                          child: Text("DAP"),
                        ),
                      ),
                      DropdownMenuItem<String>(
                        value: "3",
                        child: Center(
                          child: Text("Potash"),
                        ),
                      ),
                      DropdownMenuItem<String>(
                        value: "4",
                        child: Center(
                          child: Text("Chun"),
                        ),
                      )
                    ],
                    onChanged: (_value) => {
                      print(_value.toString()),
                      setState(() {
                        value = _value;
                      })
                    },
                    hint: Text("Select Commodity"),
                  ),
                  Text(
                    "$value",
                  ),

                  SizedBox(
                    height: 10.0,
                  ),
                  /*TextFormField(
                    controller: _bagController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'No. of Bags',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),*/
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    inputFormatters: [
                    WhitelistingTextInputFormatter.digitsOnly
                    ],
                    controller: _quantityController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Quantity in Metric Ton (M.T)',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    controller: _rateController,
                    cursorColor: Colors.green,
                    decoration: InputDecoration(
                      labelText: 'Rate',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    controller: _amountController,
                    cursorColor: Colors.green,
                    decoration: InputDecoration(
                      labelText: 'Amount',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _bank_idController,
                    cursorColor: Colors.indigo,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Bank ID / Managable ID',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),

                  /* Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: rice,
                      onChanged: onChangedRice,
                      activeColor: Colors.green[900],
                      title: const Text("Rice"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: wheat,
                      onChanged: onChangedWheat,
                      activeColor: Colors.green[900],
                      title: const Text("Wheat"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: maize,
                      onChanged: onChangedMaize,
                      activeColor: Colors.green[900],
                      title: const Text("Maize"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: mustard,
                      onChanged: onChangedMustard,
                      activeColor: Colors.green[900],
                      title: const Text("Mustard"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: others,
                      onChanged: onChangedOthers,
                      activeColor: Colors.green[900],
                      title: const Text("Others"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),*/
                  /*TextFormField(
                    controller: _purposeController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Purpose',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),*/
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _notesController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Notes',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),

                  TextFormField(
                    controller: _action_dateController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Date',
                        hintText: "Date here",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                    onTap: () async {
                      DateTime date = DateTime(1900);
                      FocusScope.of(context).requestFocus(new FocusNode());

                      date = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(1900),
                          lastDate: DateTime(2100));
                      builder:
                      (BuildContext context, Widget child) {
                        return Theme(
                          data: ThemeData(
                            primarySwatch: Colors.green,
                            primaryColor: Color.fromRGBO(97, 112, 228, 1),
                            accentColor: Color.fromRGBO(97, 112, 228, 1),
                          ),
                          child: child,
                        );
                      };
                      _action_dateController.text = date.toIso8601String();
                    },
                  ),

                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _user_idController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: "User's ID",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),

                  Container(
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Color.fromRGBO(97, 112, 228, 1),
                        ),
                        borderRadius: BorderRadius.circular(10.0)),
                    child: DropDownField(
                      labelStyle: const TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w400,
                      ),
                      labelText: 'Commodity',
                      controller: comodityselected,
                      hintText: "Choose one",
                      enabled: true,
                      itemsVisibleInDropdown: 4,
                      items: Commodity,
                      onValueChanged: (value) {
                        setState(() {
                          choose = value;
                        });
                      },
                    ),
                  ),
                  /* AutoCompleteTextField(
                    controller: _itemController,
                    itemSubmitted: (item) {
                      _itemController.text = item;
                    },
                    clearOnSubmit: false,
                    suggestions: [
                      "1",
                      "2",
                      "3",
                      "4",
                    ],
                    style: TextStyle(color: Colors.black, fontSize: 16.0),
                    itemBuilder: (context, item) {
                      return Container(
                        padding: EdgeInsets.all(20.0),
                        child: Row(
                          children: [
                            Text(
                              item,
                              style: TextStyle(color: Colors.black),
                            ),
                          ],
                        ),
                      );
                    },
                    itemSorter: (c, d) {
                      return c.compareTo(d);
                    },
                    itemFilter: (item, query) {
                      return item.toLowerCase().startsWith(query.toLowerCase());
                    },
                    decoration: InputDecoration(
                      labelText: "Commodity",
                      hintText: "'1.Dap','2.Urea','3.Potash','4.Chun'",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),

                  ),*/
                  SizedBox(
                    height: 10.0,
                  ),
                  // ignore: missing_required_param
                  AutoCompleteTextField(
                    controller: _consumer_phoneController,
                    itemSubmitted: (item) {
                      _consumer_phoneController.text = item;
                    },
                    clearOnSubmit: false,
                    suggestions: numberList,
                    style: TextStyle(color: Colors.black, fontSize: 16.0),
                    itemBuilder: (context, item) {
                      return Container(
                        padding: EdgeInsets.all(20.0),
                        child: Row(
                          children: [
                            Text(
                              item,
                              style: TextStyle(color: Colors.black),
                            ),
                          ],
                        ),
                      );
                    },
                    itemSorter: (c, d) {
                      return c.compareTo(d);
                    },
                    itemFilter: (item, query) {
                      return item.toLowerCase().startsWith(query.toLowerCase());
                    },
                    decoration: InputDecoration(
                      labelText: "Consumer's Phone",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ), //key: null,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      loading
                          ? CircularProgressIndicator()
                          : searchTextField = AutoCompleteTextField<User>(
                              key: key,
                              clearOnSubmit: false,
                              suggestions: users,
                              style: TextStyle(
                                  color: Colors.black, fontSize: 16.0),
                              decoration: InputDecoration(
                                labelText: "Consumer's Name",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                              ),
                              itemFilter: (item, query) {
                                return item.name
                                    .toLowerCase()
                                    .startsWith(query.toLowerCase());
                              },
                              itemSorter: (a, b) {
                                return a.name.compareTo(b.name);
                              },
                              itemSubmitted: (item) {
                                setState(() {
                                  searchTextField.textField.controller.text =
                                      item.name;
                                });
                              },
                              itemBuilder: (context, item) {
                                //ui for the autocomplete row
                                return row(item);
                              },
                            ),
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _consumer_address_idController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: "Consumer's Address ID",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _consumer_citizen_idController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: "Consumer's Citizen No.",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _consumer_areaController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: "Consumer's Area Code",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    height: 50,
                    child: new FlatButton(
                      onPressed: () {
                        databaseHelper.addData(
                            _bagController.text.trim(),
                            _quantityController.text.trim(),
                            _bank_idController.text.trim(),
                            _purposeController.text.trim(),
                            _notesController.text.trim(),
                            _action_dateController.text.trim(),
                            _user_idController.text.trim(),
                            _itemController.text.trim(),
                            _consumer_nameController.text.trim(),
                            _consumer_phoneController.text.trim(),
                            _consumer_address_idController.text.trim(),
                            _consumer_citizen_idController.text.trim(),
                            _consumer_areaController.text.trim(),
                            _rateController.text.trim(),
                            _amountController.text.trim());

                        print("Rice in ${rice ? 'checked' : 'unchecked'}");
                        print("Wheat in ${wheat ? 'checked' : 'unchecked'}");
                        print("Maize in ${maize ? 'checked' : 'unchecked'}");
                        print(
                            "Mustard in ${mustard ? 'checked' : 'unchecked'}");
                        print("Others in ${others ? 'checked' : 'unchecked'}");
                      },
                      color: Colors.green,
                      child: new Text(
                        "Add",
                        style: new TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget checkbox(
      {String title, bool initValue, Function(bool boolValue) onChanged}) {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Text(
        title,
        style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
      ),
      Checkbox(
        value: initValue,
        onChanged: (b) => onChanged(b),
        activeColor: Color(0xFF1B5E20),
      )
    ]);
  }
}

/*String choose = "";

final comodityselected = TextEditingController();
// creating a list of strings for commodities
List<String> Commodity = [
  "1",
  "2",
  "3",
  "4",
];*/

class Commodity {
  const Commodity(
    this.id,
    this.name,
  );
  final String name;
  final int id;
}*/

import 'package:flutter/material.dart';
import 'package:imss/Controller/ApiHelper.dart';
import 'package:imss/pages/app_main.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:imss/FarmerAutoComplete/user.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:intl/intl.dart';

class AddSalesData extends StatefulWidget {
  AddSalesData({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _AddSalesDataState createState() => _AddSalesDataState();
}

class _AddSalesDataState extends State<AddSalesData> {

  //Multiple Check Box
  bool rice = false;
  bool wheat = false;
  bool maize = false;
  bool mustard = false;
  bool others = false;

  //CheckBox Here
  String _selected;
  List<Map> _myJson = [
    {"id": '1', "image": "assets/checkbox/rice.png", "name": "rice"},
    {"id": '2', "image": "assets/checkbox/wheat.jpeg", "name": "wheat"},
    {"id": '3', "image": "assets/checkbox/maize.jpg", "name": "maize"},
    {"id": '4', "image": "assets/checkbox/mustard.jpg", "name": "mustard"},
    {"id": '5', "image": "assets/checkbox/others.png", "name": "others"},
  ];

  //List<int> selectedValues = [];

  //Date here
  DateTime selectedDate = new DateTime.now();
  //Multiple Checkbox here
  String value = "";

  AutoCompleteTextField searchTextField;
  GlobalKey<AutoCompleteTextFieldState<User>> key = new GlobalKey();
  static List<User> users = new List<User>();
  bool loading = true;

  void getUsers() async {
    try {
      String myUrl = "http://ims.rarait.com/api/users";
      final response = await http.get(myUrl, headers: {
        "Accept": "application/json",
        "Authorization":
            'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiNTFkODQwZDhhYTNmZTUxYzhlOWE4M2E0OWYzODMwMTkzNzgxNGQ5ZjZlYmM1MmE4M2UyMzNkZjFjZTZlNGU0ZGQ0ZmM5Yjg1MzBhMjJiYTMiLCJpYXQiOjE2MTE0ODYyMTQsIm5iZiI6MTYxMTQ4NjIxNCwiZXhwIjoxNjQzMDIyMjE0LCJzdWIiOiI2NjAiLCJzY29wZXMiOltdfQ.Av7RpBzcOdy6qsCoXr9asDM43uKMTh4axbG7ATynodQfg373ZFsUa8LM_mm5TIZn6RIWOV8AW4wtFVgZqsvTZFFStA0hDwgP9Cfd6nbXJ0BxCvJGTbv_jEGO0HuQHGsOT4GDKPkI0Bt6OHStefYj6MR6WU90F-AVcu5FQ1Fr6TOqoeBulUiYSV8X-OshLSRVycx5mHqq3C6Hquz3yJuLVHzD4zmFQCNp-gUYcKylS1_wI0ch1xWmuR_jOarKe-BB2YQOq3d25PfHxG5F0OeDbLhem9xVzxXS1KitFl3pZlMSl1b0e1XFxHKbqS2Ikj_5EteCFbVZqZGza0xBIxfQcmG2a_dtHUDjKSDk8_wZgh5GyOy1Y00ZHSn33Z5r4QwZPkK-RLHEtS69rpbxxf2uiTEaPRjyZg2aZOCVRkUXCAZfuGSl5JfuwYuq_SjPK6MH04_l49q5Grsa-CiJu2y288U8HGXUvt5vjbJAXJC06b04bcjxBDKhuFIEH4Y2rjQi6ej_LcJ5RQ__S-B3_y7ucSykVBU6bPDr_40bbwlTiuxvG5YXLZDs4qYjPJG4qjdtN2rhsqddwf3aqeQ9ACSEEOItKDiVvtw_1DHItvxCWvQujZ9FAituTPXNxO1vp7JiQ25glc2tmP1DaaPIcwK5xIJvjZ-ZfGooVe4yXsXV5Ew',});
      if (response.statusCode == 200) {
        users = loadUsers(response.body);
        print('Users: ${users.length}');
        setState(() {
          loading = false;
        });
      } else {
        //_showFloatingFlushbarSales();
        print("Error getting users");
      }
    } catch (e) {
      //_showFloatingFlushbarSales();
      print("Error getting users");
    }
  }

  static List<User> loadUsers(String jsonString) {
    final parsed = json.decode(jsonString).cast<Map<String, dynamic>>();
    return parsed.map<User>((json) => User.fromJson(json)).toList();
  }

  @override
  void initState() {
    getUsers();
    super.initState();
  }

  Widget row(User user) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          user.name,
          style: TextStyle(fontSize: 16.0),
        ),
        SizedBox(
          width: 10.0,
        ),
        Text(
          user.phone,
        ),
      ],
    );
  }

  /*void onChangedRice(bool value) {
    setState(() {
      rice = value;
    });
  }

  void onChangedWheat(bool value) {
    setState(() {
      wheat = value;
    });
  }

  void onChangedMaize(bool value) {
    setState(() {
      maize = value;
    });
  }

  void onChangedMustard(bool value) {
    setState(() {
      mustard = value;
    });
  }

  void onChangedOthers(bool value) {
    setState(() {
      others = value;
    });
  }*/

  DatabaseHelper databaseHelper = new DatabaseHelper();

  //final TextEditingController _bagController = new TextEditingController();

  //final TextEditingController _quantityController = new TextEditingController();
  final TextEditingController _bank_idController = new TextEditingController();
  final TextEditingController _purposeController = new TextEditingController();
  final TextEditingController _notesController = new TextEditingController();
  final TextEditingController _action_dateController = new TextEditingController();
  //final TextEditingController _user_idController = new TextEditingController();
  final TextEditingController _itemController = new TextEditingController();
  final TextEditingController _consumer_nameController =
      new TextEditingController();
  final TextEditingController _consumer_phoneController =
      new TextEditingController();
  final TextEditingController _consumer_address_idController =
      new TextEditingController();
  final TextEditingController _consumer_citizen_idController =
      new TextEditingController();

  // ignore: non_constant_identifier_names
  final TextEditingController _consumer_areaController =
      new TextEditingController();
  final TextEditingController _rateController = new TextEditingController();
  final TextEditingController _amountController = new TextEditingController();
  final TextEditingController _kgController = new TextEditingController();

  GlobalKey<FormState> salesformkey = GlobalKey<FormState>();

  List numberList = [
    "9842049245",
    "9842365122",
    "9842181991",
    "9842198093",
    "9810310253",
    "9819068807",
    "9842176429",
    "9842585129",
    "9842091919",
    "9805384209",
    "9811039977",
    "9808076397",
    "9812396164",
    "9827023481",
    "9882222959",
    "9825357975",
    "9813033463",
    "9813072296",
    "9819086452",
    "9827357827",
    "9817318116",
    "9805327043",
    "9814321932",
    "9811058063",
    "9842136116",
    "9852080873",
    "9817238355",
    "9862143854",
    "9817375924",
    "9818315781",
    "9842086406",
    "9842113103",
    "9842071906",
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Add Sales",
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.home,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => new FirstPage()));
            },
          ),
          backgroundColor: Color(0xFF1B5E20),
          title: Text('Add Sales'),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.refresh,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => new AddSalesData()));
              },
            ),
          ],
        ),
        body: Container(
          padding: const EdgeInsets.symmetric(vertical: 9.0, horizontal: 25.0),
          child: Form(
            key: salesformkey,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    child: Text(
                      'Purpose',
                      style: TextStyle(
                        color: Colors.green[900],
                        fontSize: 16.0,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  /* MultiSelect(
                      autovalidate: false,
                      titleText: title,
                      validator: (value) {
                        if (value == null) {
                          return 'Please select one or more option(s)';
                        }
                      },
                      errorText: 'Please select one or more option(s)',
                      dataSource: [
                        {
                          "display": "rice",
                          "value": 1,
                        },
                        {
                          "display": "wheat",
                          "value": 2,
                        },
                        {
                          "display": "maize",
                          "value": 3,
                        },
                        {
                          "display": "mustard",
                          "value": 4,
                        },
                        {
                          "display": "others",
                          "value": 5,
                        }
                      ],
                      textField: 'display',
                      valueField: 'value',
                      filterable: true,
                      required: true,
                      value: null,
                      onSaved: (value) {
                        print('The value is $_purposeController',);
                      }
                  ),*/
                  /* Container(
                   padding: EdgeInsets.all(15),
                   decoration: BoxDecoration(
                     border: Border.all(width: 1, color: Colors.grey),
                     borderRadius: BorderRadius.circular(10)
                   ),
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: [
                       Expanded(
                         child: DropdownButtonHideUnderline(
                           child: ButtonTheme(
                             alignedDropdown: true,
                             child: DropdownButton<String>(
                               isDense: true,
                               hint: new Text("Select Purpose"),
                               value: _selected,
                               onChanged: (String newValue){
                                 setState(() {
                                   _selected = newValue;
                                 });
                                 print(_selected);
                               },
                               items: _myJson.map((Map map) {
                                 return new DropdownMenuItem<String>(
                                   value: map["name"].toString(),
                                   //value: _mySelection,
                                   child: Row(
                                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                     children: [
                                       Image.asset(
                                         map["image"],
                                         width: 25,
                                       ),
                                       Container(
                                         margin: EdgeInsets.only(left: 10),
                                         child: Text(map["name"]),
                                       ),
                                       Checkbox(
                                         value: false,
                                         onChanged: (val){
                                           print("val");
                                         },
                                       ),

                                     ],
                                   ),
                                 );
                               }).toList(),
                             ),
                           ),
                         ),
                       ),
                     ],

                   ),
                 ),*/
                  //Multiple Checkbox body part
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      checkbox(
                          title: "rice",
                          initValue: rice,
                          onChanged: (sts) => setState(() => rice = sts)),
                      SizedBox(
                        width: 15.0,
                      ),
                      checkbox(
                          title: "wheat",
                          initValue: wheat,
                          onChanged: (sts) => setState(() => wheat = sts)),
                      SizedBox(
                        width: 15.0,
                      ),
                      checkbox(
                          title: "maize",
                          initValue: maize,
                          onChanged: (sts) => setState(() => maize = sts)),
                      SizedBox(
                        width: 15.0,
                      ),
                      checkbox(
                          title: "mustard",
                          initValue: mustard,
                          onChanged: (sts) => setState(() => mustard = sts)),
                      SizedBox(
                        width: 15.0,
                      ),
                      checkbox(
                          title: "others",
                          initValue: others,
                          onChanged: (sts) => setState(() => others = sts)),
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  /*TextFormField(
                    controller: _itemController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Items ID (1/2/3/4)',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),*/
                  /* Container(
                    child: DropdownButton<String>(
                      items: [
                        DropdownMenuItem<String>(
                          value: "1",
                          child: Center(
                            child: Text("Urea"),
                          ),
                        ),
                        DropdownMenuItem<String>(
                          value: "2",
                          child: Center(
                            child: Text("DAP"),
                          ),
                        ),
                        DropdownMenuItem<String>(
                          value: "3",
                          child: Center(
                            child: Text("Potash"),
                          ),
                        ),
                        DropdownMenuItem<String>(
                          value: "4",
                          child: Center(
                            child: Text("Chun"),
                          ),
                        )
                      ],
                      onChanged: (_value) => {
                        print(_value.toString()),
                        setState(() {
                          value = _value;
                        })
                      },
                      hint: TextFormField(
                        controller: _itemController,
                        cursorColor: Colors.green,
                        decoration: InputDecoration(
                            labelText: "$value",
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            )),
                      ),
                    ),
                  ),*/
                  /*Container(
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Color.fromRGBO(97, 112, 228, 1),
                        ),
                        borderRadius: BorderRadius.circular(10.0)),
                    child: DropDownField(
                      labelStyle: const TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w400,
                      ),
                      labelText: 'Commodity',
                      controller: _itemController,
                      hintText: "Choose one",
                      enabled: true,
                      itemsVisibleInDropdown: 4,
                      items: Commodity,
                      onValueChanged: (value) {
                        setState(() {
                          choose = value;
                        });
                      },
                    ),
                  ),*/
                   /*TextFormField(
                    controller: _itemController,
                    decoration: InputDecoration(
                        labelText: "Type this ID number : $value",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                    /* child: Text(
                      "$value",
                    ),*/
                  ),*/
                  SizedBox(
                    height: 5.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'K.G',
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF1B5E20),
                            ),
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Container(
                            width: 155.0,
                            height: 40.0,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(17.0),
                            ),
                            child: TextFormField(
                              controller: _kgController,
                              cursorColor: Colors.green,
                              //validator: validatefarmer,
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.fromLTRB(
                                      20.0, 10.0, 20.0, 10.0),
                                  labelText: "Enter K.G",
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  )),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        height: 50.0,
                        color: Colors.grey,
                        width: 1.0,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Rate',
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF1B5E20),
                            ),
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Container(
                            width: 155.0,
                            height: 40.0,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(17.0),
                              //color: Color(0xFF1B5E20),
                            ),
                            child: TextFormField(
                              controller: _rateController,
                              cursorColor: Colors.green,
                              //validator: validatefarmer,
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.fromLTRB(
                                      20.0, 10.0, 20.0, 10.0),
                                  labelText: "Enter rate",
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  )),
                            ),
                            /*child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                InkWell(
                                  onTap: () {},
                                  child: Container(
                                    height: 25.0,
                                    width: 25.0,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(7.0),
                                      color: Color(0xFF1B5E20),
                                    ),
                                    child: Center(
                                      child: Icon(
                                        Icons.remove,
                                        color: Colors.white,
                                        size: 20.0,
                                      ),
                                    ),
                                  ),
                                ),
                                Text(
                                  '120',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 15.0,
                                  ),
                                ),
                                InkWell(
                                  onTap: () {},
                                  child: Container(
                                    height: 25.0,
                                    width: 25.0,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(7.0),
                                      color: Colors.white,
                                    ),
                                    child: Center(
                                      child: Icon(
                                        Icons.add,
                                        color: Color(0xFF1B5E20),
                                        size: 20.0,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),*/
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Commodity',
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF1B5E20),
                            ),
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Container(
                            width: 155.0,
                            height: 40.0,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(17.0),
                            ),
                            /*child: TextFormField(
                              controller: _itemController,
                              cursorColor: Colors.green,
                              //validator: validatefarmer,
                              decoration: InputDecoration(
                                  labelText: 'Items ID (1/2/3/4)',
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  )),
                            ),*/
                            child: DropdownButton<String>(
                              isExpanded: true,
                              items: [
                                DropdownMenuItem<String>(
                                  value: "1",
                                  child: Center(
                                    child: Text("Urea"),
                                  ),
                                ),
                                DropdownMenuItem<String>(
                                  value: "2",
                                  child: Center(
                                    child: Text("DAP"),
                                  ),
                                ),
                                DropdownMenuItem<String>(
                                  value: "3",
                                  child: Center(
                                    child: Text("Potash"),
                                  ),
                                ),
                                DropdownMenuItem<String>(
                                  value: "4",
                                  child: Center(
                                    child: Text("Chun"),
                                  ),
                                )
                              ],
                              onChanged: (_value) => {
                                print(_value.toString()),
                                setState(() {
                                  value = _value;
                                })
                              },
                              hint: TextFormField(
                                //initialValue: '$value',
                                controller: _itemController,
                                cursorColor: Colors.green,
                                decoration: InputDecoration(
                                    labelText: "$value",
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    )),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        height: 50.0,
                        color: Colors.grey,
                        width: 1.0,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Total Amount',
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF1B5E20),
                            ),
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Container(
                            width: 155.0,
                            height: 40.0,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(17.0),
                            ),
                            child: TextFormField(
                              controller: _amountController,
                              cursorColor: Colors.green,
                              //validator: validatefarmer,
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.fromLTRB(
                                      20.0, 10.0, 20.0, 10.0),
                                  labelText: "Enter Amount",
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  /*TextFormField(
                    controller: _quantityController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        labelText: 'Quantity in Metric Ton (M.T)',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),*/
                  /*TextFormField(
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    controller: _rateController,
                    cursorColor: Colors.green,
                    decoration: InputDecoration(
                      labelText: 'Rate',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),*/
                  /*TextFormField(
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    controller: _amountController,
                    cursorColor: Colors.green,
                    decoration: InputDecoration(
                      labelText: 'Amount',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),*/
                  TextFormField(
                    controller: _bank_idController,
                    cursorColor: Colors.indigo,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        labelText: 'Bank ID / Managable ID',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  /*Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: rice,
                      onChanged: onChangedRice,
                      activeColor: Colors.green[900],
                      title: const Text("Rice"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: wheat,
                      onChanged: onChangedWheat,
                      activeColor: Colors.green[900],
                      title: const Text("Wheat"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: maize,
                      onChanged: onChangedMaize,
                      activeColor: Colors.green[900],
                      title: const Text("Maize"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: mustard,
                      onChanged: onChangedMustard,
                      activeColor: Colors.green[900],
                      title: const Text("Mustard"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: others,
                      onChanged: onChangedOthers,
                      activeColor: Colors.green[900],
                      title: const Text("Others"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),*/
                  /*TextFormField(
                    controller: _purposeController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Purpose',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),*/
                  /* TextFormField(
                    controller: _action_dateController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Date',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),*/
                  /*TextFormField(
                    controller: _user_idController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,

                    decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        labelText: "User's ID",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),*/
                  /*TextFormField(
                    controller: _bagController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        labelText: 'No. of Bags',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),*/
                  /*TextFormField(
                    controller: _itemController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Items ID (1/2/3/4)',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),*/
                  /*Container(
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Color.fromRGBO(97, 112, 228, 1),
                        ),
                        borderRadius: BorderRadius.circular(10.0)),
                    child: DropDownField(
                      labelStyle: const TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w400,
                      ),
                      labelText: 'Commodity',
                      controller: comodityselected,
                      hintText: "Choose one",
                      enabled: true,
                      itemsVisibleInDropdown: 4,
                      items: Commodity,
                      onValueChanged: (value) {
                        setState(() {
                          choose = value;
                        });
                      },
                    ),
                  ),*/
                  // ignore: missing_required_param
                  AutoCompleteTextField(
                    controller: _consumer_phoneController,
                    itemSubmitted: (item) {
                      _consumer_phoneController.text = item;
                    },
                    clearOnSubmit: false,
                    suggestions: numberList,
                    style: TextStyle(color: Colors.black, fontSize: 16.0),
                    itemBuilder: (context, item) {
                      return Container(
                        padding: EdgeInsets.all(20.0),
                        child: Row(
                          children: [
                            Text(
                              item,
                              style: TextStyle(color: Colors.black),
                            ),
                          ],
                        ),
                      );
                    },
                    itemSorter: (c, d) {
                      return c.compareTo(d);
                    },
                    itemFilter: (item, query) {
                      return item.toLowerCase().startsWith(query.toLowerCase());
                    },
                    decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      labelText: "Consumer's Phone",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ), //key: null,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      loading
                          ? CircularProgressIndicator()
                          : searchTextField = AutoCompleteTextField<User>(
                              key: key,
                              clearOnSubmit: false,
                              suggestions: users,
                              style: TextStyle(
                                  color: Colors.black, fontSize: 16.0),
                              decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                                labelText: "Consumer's Name",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                              ),
                              itemFilter: (item, query) {
                                return item.name
                                    .toLowerCase()
                                    .startsWith(query.toLowerCase());
                              },
                              itemSorter: (a, b) {
                                return a.name.compareTo(b.name);
                              },
                              itemSubmitted: (item) {
                                setState(() {
                                  searchTextField.textField.controller.text =
                                      item.name;
                                });
                              },
                              itemBuilder: (context, item) {
                                //ui for the autocomplete row
                                return row(item);
                              },
                            ),
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  /*TextFormField(
                    controller: _notesController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        labelText: 'Land Area',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),*/
                  TextFormField(
                    controller: _consumer_citizen_idController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        labelText: "Consumer's Citizen No.",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _consumer_address_idController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        labelText: "Consumer's Address ID",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _consumer_areaController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        labelText: "Ward No.",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  /*TextFormField(
                    controller: _action_dateController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        labelText: "Enter Date",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),*/

                  //------------------Date-----------------------
                  TextFormField(
                    controller: _action_dateController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        labelText: 'Date',
                        hintText: "Date here",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                    onTap: () async {
                      final DateTime now = DateTime.now();
                      final DateFormat formatter = DateFormat('yyyy-MM-dd');
                      final String formatted = formatter.format(now);
                      print(formatted);
                      DateTime picked = DateTime(1990);

                      FocusScope.of(context).requestFocus(new FocusNode());

                      picked = await showDatePicker(
                        context: context,
                        initialDate: selectedDate,
                        firstDate: DateTime(2000),
                        lastDate: DateTime(2025),
                        initialEntryMode: DatePickerEntryMode.input,
                        errorFormatText: 'Enter valid date',
                      );
                      builder:
                          (BuildContext context, Widget child) {
                        return Theme(
                          data: ThemeData(
                            primarySwatch: Colors.green,
                            primaryColor: Color.fromRGBO(12, 112, 228, 1),
                            accentColor: Color.fromRGBO(97, 112, 228, 1),
                          ),
                          child: child,
                        );
                      };
                      _action_dateController.text = picked.toString();
                      if (picked != null && picked != selectedDate)
                        setState(() {
                          selectedDate = picked;
                          //child: Text("${picked.year}-${picked.month}-${picked.day}", style: TextStyle(fontSize: 32.0),);
                        });
                    },
                  ),
                 SizedBox(
                   height: 10.0,
                 ),
                 /*TextFormField(
                    controller: _action_dateController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        labelText: 'Date',
                        hintText: "Date here",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                    onTap: () async {
                      final DateTime now = DateTime.now();
                      final DateFormat formatter = DateFormat('yyyy-MM-dd');
                      final String formatted = formatter.format(now);
                      print(formatted);
                      DateTime date = DateTime(1900);
                      FocusScope.of(context).requestFocus(new FocusNode());
                      date = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2019, 1),
                          lastDate: DateTime(2021, 12));

                      builder:
                          (BuildContext context, Widget child) {
                        return Theme(
                          data: ThemeData(
                            primarySwatch: Colors.green,
                            primaryColor: Color.fromRGBO(12, 112, 228, 1),
                            accentColor: Color.fromRGBO(97, 112, 228, 1),
                          ),
                          child: child,
                        );
                      };
                      _action_dateController.text = date.toIso8601String();
                    },
                  ),*/
                  TextFormField(
                    controller: _notesController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        labelText: 'Remarks',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    height: 50,
                    child: new FlatButton(
                      onPressed: () {
                        databaseHelper.addData(
                            //_bagController.text.trim(),
                            //_quantityController.text.trim(),
                            _bank_idController.text.trim(),
                            _purposeController.text.trim(),
                            _notesController.text.trim(),
                            _action_dateController.text.trim(),
                            //_user_idController.text.trim(),
                            _itemController.text.trim(),
                            _consumer_nameController.text.trim(),
                            _consumer_phoneController.text.trim(),
                            _consumer_address_idController.text.trim(),
                            _consumer_citizen_idController.text.trim(),
                            _consumer_areaController.text.trim(),
                            _rateController.text.trim(),
                            _amountController.text.trim(),
                            _kgController.text.trim());

                        print("Rice in ${rice ? 'checked' : 'unchecked'}");
                        print("Wheat in ${wheat ? 'checked' : 'unchecked'}");
                        print("Maize in ${maize ? 'checked' : 'unchecked'}");
                        print(
                            "Mustard in ${mustard ? 'checked' : 'unchecked'}");
                        print("Others in ${others ? 'checked' : 'unchecked'}");
                      },
                      color: Colors.green,
                      child: new Text(
                        "Save Changes",
                        style: new TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

// Multiple CheckBox
  Widget checkbox(
      {String title, bool initValue, Function(bool boolValue) onChanged}) {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Text(
        title,
        style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
      ),
      Checkbox(
        value: initValue,
        onChanged: (b) => onChanged(b),
        activeColor: Color(0xFF1B5E20),
      )
    ]);
  }
/*void _showFloatingFlushbarSales() {
    Flushbar(
      duration: Duration(seconds: 2),
      padding: EdgeInsets.all(20),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.green.shade800, Colors.green],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.white,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      title: 'Sales entry Successfull',
      message: 'Continue with your next entry',
      icon: Icon(
        Icons.info_outline,
        size: 30,
        color: Colors.white,
      ),
    )..show(context);
  }*/
}

String select = "";
String choose = "";

final comodityselected = TextEditingController();

// creating a list of strings for commodities
List<String> Commodity = [
  "1",
  "2",
  "3",
  "4",
];

